package mediautil;

import mediautil.image.jpeg.Entry;
import mediautil.image.jpeg.Exif;
import mediautil.image.jpeg.LLJTran;
import mediautil.image.jpeg.LLJTranException;
import mediautil.test.LLJTranTester;
import org.junit.Test;

import java.io.*;

/**
 * xxx
 *
 * @author maguoyong
 * @date 2020/12/6
 */
public class JpgTest {



    @Test
    public void test1() throws IOException, LLJTranException {
        InputStream is = new FileInputStream(new File("/Users/gaoyuan/Desktop/原图1.jpg"));
        LLJTran llj = new LLJTran(is);
        try {
            llj.read(LLJTran.READ_INFO, true);
        } catch (LLJTranException e) {
            e.printStackTrace();
        }

        Exif exif = (Exif) llj.getImageInfo();


        Entry entry = exif.getTagValue(Exif.USERCOMMENT, true);
        entry.setValue(0, "guanyunkeji");
        llj.refreshAppx();
        FileOutputStream nhop = new FileOutputStream("/Users/gaoyuan/Desktop/原图1_exif.jpg");
        llj.xferInfo(null, nhop, LLJTran.REPLACE, LLJTran.REPLACE);
        nhop.close();
        llj.freeMemory();

    }


    @Test
    public  void test2() throws Exception {
        LLJTran llj = new LLJTran(new File("/Users/gaoyuan/Downloads/mediautil-1.0/docs/tutorials/LLJTran/image.jpg"));
        LLJTranTester.readImage(llj, true, LLJTran.READ_INFO, 0, 0);
        // llj.closeInternalInputStream();
        String newTime = "";
        Exif exif = (Exif) llj.getImageInfo();

        Entry entry = exif.getTagValue(Exif.DATETIME, true);
        if(entry != null)
            entry.setValue(0, "1998:08:18 11:15:00");
        entry = exif.getTagValue(Exif.DATETIMEORIGINAL, true);
        if(entry != null)
            entry.setValue(0, "1998:08:18 11:15:00");
        entry = exif.getTagValue(Exif.DATETIMEDIGITIZED, true);
        if(entry != null)
            entry.setValue(0, "1998:08:18 11:15:00");
        entry = exif.getTagValue(Exif.ORIENTATION, true);
        System.out.println("Orient Entry = " + entry);
        if(entry != null)
            entry.setValue(0, new Integer(6));

        llj.refreshAppx();

        // FileInputStream fip = new FileInputStream("Img_1217.jpg");
        FileOutputStream nhOp = new FileOutputStream("del.jpg");
        llj.xferInfo(null, nhOp, LLJTran.REPLACE, LLJTran.REPLACE);
        // fip.close();
        nhOp.close();
//        llj.read(LLJTran.READ_ALL, true);
//        FileOutputStream op = new FileOutputStream("/Users/gaoyuan/Desktop/原图1_exif.jpg");
//        int transformOp = Integer.parseInt(args[2]);
//        llj.transform(op, transformOp);
//        op.close();
    }
}
